package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String API_BASE_URL="/apitechu/v1";

    @GetMapping(API_BASE_URL + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel upProduct , @PathVariable String id){
        System.out.println("Update Product");
        System.out.println("updated ID: " + upProduct.getId());
        System.out.println("updated DESC: " + upProduct.getDesc() );
        System.out.println("updated PRICE: " + upProduct.getPrice() );

        for (ProductModel productinList : ApitechuApplication.productModels)
            if (productinList.getId().equals(id))
            {
                productinList.setId(upProduct.getId());
                productinList.setDesc(upProduct.getDesc());
                productinList.setPrice(upProduct.getPrice());
            }
        return upProduct;
    }

    // Path Variable le indica es parte de la URL
    // por ejemplo http://localhost:8080/apitechu/v1/products/2
    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductsByID");
        System.out.println("ID seleccionado " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels)
            if (product.getId().equals(id))
            {
                result = product;
            }

        return result;
    }

    // Cuidado el el post mapping implica que ya esta creada la url
    @PostMapping(API_BASE_URL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct ){
        System.out.println("Create Product");
        System.out.println("ID: " + newProduct.getId());
        System.out.println("DESC: " + newProduct.getDesc() );
        System.out.println("PRICE: " + newProduct.getPrice() );

        ApitechuApplication.productModels.add(newProduct);

        return ( newProduct);

    }

    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("Delete Product");
        System.out.println("LA id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productinList : ApitechuApplication.productModels)
            if (productinList.getId().equals(id))
            {
                System.out.println("Producto "+ id +" encontrado");
                foundProduct = true;
                result = productinList;
            }

        if (foundProduct == true) {
            ApitechuApplication.productModels.remove(result);
            System.out.println("Producto "+ id +" borrado");
        }

        return result ;
    }

    // PATCH solo precio y/o la desc pero no el id y tiene que comprobar que no vienen de mas
   // nada ok , desc ok, desc y pr ok si vienen las 3 no ok
    // asumo que el id tiene que venir a null porque lo cojo de la url
   @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel patchedProduct, @PathVariable String id){
       System.out.println("Patch Product");
       System.out.println("patched ID: " + id);

       ProductModel result = new ProductModel();

       for (ProductModel productinList : ApitechuApplication.productModels){
           if (productinList.getId().equals(id))  {
               result = productinList;

                // Cuidado que el tema de los params detecta mayusc minusc.
               if ( patchedProduct.getDesc() != null ){
                   productinList.setDesc(patchedProduct.getDesc());
                   System.out.println("Patched description id " + id);
               }

               if (patchedProduct.getPrice() > 0 ){
                   productinList.setPrice(patchedProduct.getPrice());
                   System.out.println("Patched price id " + id);
               }
           }
       }

//        if (patchedProduct.getId().isEmpty()){
//            // Buscar ID
//           for (ProductModel productinList : ApitechuApplication.productModels)
//               if (productinList.getId().equals(id))
//               {
//                   System.out.println("Producto "+ id +" encontrado");
//                   patchedProduct.setId(productinList.getId());
//                   // Aqui realizamos el update
//                   if ( patchedProduct.getDesc().isEmpty() )
//                        patchedProduct.setDesc(productinList.getDesc());
//
//                   if (patchedProduct.getPrice() == 0.0)
//                       patchedProduct.setPrice(productinList.getPrice());
//
//               }}

        return result ;
   }

}
