package com.techu.apitechu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {



    // llamada con http://localhost:8080/hello?name=PEPE o http://localhost:8080/hello (con o sin  ?)
    // esta por deecto a blanco
    @RequestMapping("/hello")
    public String hello(@RequestParam(value="name", defaultValue = "TechU") String name_param) {
        return String.format("Hola %s!",name_param);
    }

    // esto define una url BASE+DIR http://localhost:8080/
    @RequestMapping("/")
    public String index() {
        return "Hola mundo desde API TECH-U!";
    }


}
