package com.techu.apitechu.models;

public class ProductModel {
    // Atributos de la clase
    private String id;
    private String desc;
    private float price;

    // Constructores en generico con btn derecho y generate constructor
    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public ProductModel() {
    }

    public String getId() {
        return this.id;
    }

    public String getDesc() {
        return this.desc;
    }

    public float getPrice() {
        return this.price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
