package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

//URL de nuestra base http://localhost:8080

@SpringBootApplication
@RestController
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.productModels = ApitechuApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		// Ponemos 3 objetos de prueba
		productModels.add(new ProductModel("1","Producto1",10));
		productModels.add(new ProductModel("2","Producto2",20));
		productModels.add(new ProductModel("3","Producto3",30));

		// Finalmente devolvemos el arraylist con 3 objetos
		return productModels;
	}
}
